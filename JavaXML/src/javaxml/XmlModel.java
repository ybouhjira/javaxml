package javaxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlModel implements TableModel {

    private Document doc;
    private ArrayList<TableModelListener> listeners;

    public XmlModel() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        listeners = new ArrayList<>();
    }
    
    public Document getDoc() {
        return doc;
    }

    public void setDoc(Document doc) {
        this.doc = doc;
    }

    @Override
    public int getRowCount() {
        NodeList list = doc.getElementsByTagName("Person");
        return list.getLength();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) 
            throws IndexOutOfBoundsException
    {
        switch(columnIndex) {
            case 0:
                return "Name";
            case 1:
                return "Age";
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return new String().getClass();
            case 1:
                return new Integer(0).getClass();
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        NodeList list = doc.getElementsByTagName("Person");
        NodeList properties = list.item(rowIndex).getChildNodes();
        System.out.println(properties.getLength());
        
        
        String content = properties.item(columnIndex).getTextContent();
        
        return content;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        NodeList elements = doc.getElementsByTagName("Person");
        NodeList properties = elements.item(rowIndex).getChildNodes();
        Element e = (Element) properties.item(columnIndex);
        
        e.setNodeValue((String)aValue);
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
    
    public void readFromFile(String fileName) 
            throws ParserConfigurationException, SAXException, IOException {
        File file = new File(fileName);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.parse(file);
        
        // construire un evenement
        TableModelEvent event = new TableModelEvent(this);
        
        for(TableModelListener lst : listeners) {
            lst.tableChanged(event);
        }
    }
    
    public void writeToFile(String fileName) 
            throws TransformerConfigurationException, TransformerException {
        File file = new File(fileName);
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }
}
