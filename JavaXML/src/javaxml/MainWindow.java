
package javaxml;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;


public class MainWindow extends JFrame {
    private TableView view;
    private XmlModel model;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu person;
    private JMenuItem open;
    private JMenuItem save;
    private JMenuItem add;
    private JMenuItem remove;
    
    public MainWindow(String title) throws HeadlessException, ParserConfigurationException {
        super(title);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 300);
        
        model = new XmlModel();
        view = new TableView(model);
        
        getContentPane().add(view);
        
        menuBar = new JMenuBar();
        fileMenu = new JMenu("File");
        person = new JMenu("Person");
        
        open = new JMenuItem("Open");
        save = new JMenuItem("Save");
        add = new JMenuItem("Add");
        remove = new JMenuItem("Remove");
        
        fileMenu.add(open);
        fileMenu.add(save);
        
        person.add(add);
        person.add(remove);
        
        menuBar.add(fileMenu);
        menuBar.add(person);
        setJMenuBar(menuBar);
        
        open.addActionListener(new OpenListener());
        
    }
    
    private class OpenListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser chooser = new JFileChooser();
            // affichage du chooser
            int returnVal = chooser.showOpenDialog(MainWindow.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                String fileName = chooser.getSelectedFile().getPath();
                try {
                    model.readFromFile(fileName);
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    System.err.println("Erreur de lecture");
                }
            }
        }
    }
    
}
